import { useEffect, useRef, useState } from 'react';
import './App.scss'; 
import Button from "./components/Button"; 
import Modal from "./components/Modal";
import ProductCard from './components/productCard/ProductCard';
import Header from './components/header/Header';

function App() {
    const [isDeleteModalOpen, setIsDeleteModalOpen] = useState(false);
    const [isAddModalOpen, setIsAddModalOpen] = useState(false);
    const [products, setProducts] = useState([]);
    const selectedProductsId = useRef([]);
    const addedToCartProductsId = useRef([]);

    const [selectedProductsCount, setSelectedProductsCount] = useState(0);
    const [addedToCartProductsCount, setAddedToCartProductsCount] = useState(0);

    useEffect(() => {
        const selectedProducts = JSON.parse(localStorage.getItem('selectedProducts')) || [];
        const addedToCartProducts = JSON.parse(localStorage.getItem('addedToCartProducts')) || [];
        setSelectedProductsCount(selectedProducts.length);
        setAddedToCartProductsCount(addedToCartProducts.length);
    }, []);

    function handleAddToCart(){
        closeModal();
        console.log(addedToCartProductsId);
        let alreadyAddedToCartProducts = [];

        if (localStorage.getItem('addedToCartProducts')) {
            alreadyAddedToCartProducts = JSON.parse(localStorage.getItem('addedToCartProducts'));
            if(alreadyAddedToCartProducts.includes(addedToCartProductsId.current)) {
                alert('You`ve already added this product to a cart!');
                return
            }
        }
        localStorage.setItem('addedToCartProducts', JSON.stringify([...alreadyAddedToCartProducts, addedToCartProductsId.current]));

        updateAddedToCartProductsCount();
    }

    function updateSelectedProductsCount() {
        const selectedProducts = JSON.parse(localStorage.getItem('selectedProducts'));
        setSelectedProductsCount(selectedProducts.length);
    }


    function updateAddedToCartProductsCount() {
        const addedToCartProducts = JSON.parse(localStorage.getItem('addedToCartProducts'));
        setAddedToCartProductsCount(addedToCartProducts.length);
    }

    useEffect(()=>{
        let selectedProductsFromStorage = [];
        if(localStorage.getItem('selectedProducts')){
            selectedProductsFromStorage = JSON.parse(localStorage.getItem('selectedProducts'));
        }
        console.log('Функция selProds сработала');
        selectedProductsId.current = selectedProductsFromStorage;
        console.log(selectedProductsFromStorage);
        console.log(selectedProductsId.current);
    }, [])

    const scrollY = useRef(0); 

    function openDeleteModal(){
        setIsDeleteModalOpen(true);
    }

    function openAddModal(event, id){
        scrollY.current = window.scrollY;
        setIsAddModalOpen(true);
        document.body.style.position = 'fixed';
        document.body.style.top = `-${scrollY.current}px`;
        document.body.style.paddingLeft = '32px';
        
        addedToCartProductsId.current = id;
        console.log(addedToCartProductsId.current);
    }

    function closeModal(){
        setIsDeleteModalOpen(false);
        setIsAddModalOpen(false);
        document.body.style.position = '';
        document.body.style.top = ``;
        window.scrollTo(0, scrollY.current);
        document.body.style.paddingLeft = '';
    }

    useEffect(()=>{
        fetch('products.json')
        .then(res => res.json())
        .then(data => setProducts(data.products))
        .catch(error => console.log('An error occured while fetching all products: ', error))
    }, [])

    return (
        <div className='App' >
            <Header selectedProductsCount={selectedProductsCount} 
                    addedToCartProductsCount={addedToCartProductsCount}
            />
            <div class="cardsWrapper">
                {products.map((product) => <ProductCard
                                                key={product.setNumber}
                                                updateSelectedProductsCount={updateSelectedProductsCount}
                                                selectedProductsId={selectedProductsId.current}
                                                id={product.id}
                                                name={product.name}
                                                price={product.price}
                                                description={product.description}
                                                color={product.color}
                                                imgUrl={product.imgUrl}
                                                openAddModal={openAddModal}
                                            />)}
            </div>
            {isDeleteModalOpen && 
                <Modal
                backgroundColor='linear-gradient(to bottom, #D44638 24%, #E74C3D 24% 100%)'
                header='Do you want to delete this file?'
                closeButton='true'
                text='Once you delete this file it won`t be possible to undo this action.
                Are you sure you want to delete it?'
                actions={<><button className='buttonForDeleteModal'>Ok</button>
                <button className='buttonForDeleteModal'>Cancel</button></>}
                closeModal={closeModal}
            />
            }

            {isAddModalOpen && 
                <Modal
                backgroundColor='linear-gradient(to bottom, #348a0c 33%, #269618 24% 100%)'
                header='Do you want to add this product to your cart?'
                closeButton='true'
                text='If you choose No you, you can still add it later'
                actions={<><button className='buttonForAddModal' onClick={handleAddToCart}>Add</button>
                <button className='buttonForAddModal' onClick={closeModal}>No</button></>}
                closeModal={closeModal}
            />
            }
            
            <div className="modalButtonsWrapper" style={{display: 'none'}}>
                <Button
                    backgroundColor='#E74C3D'
                    text='Open first modal'
                    onClick={openDeleteModal}
                />
                <Button
                    backgroundColor='#269618'
                    text='Open second modal'
                    onClick={openAddModal}
                />
            </div>
        </div>
    );
}

export default App;