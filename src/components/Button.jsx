import styles from './Button.module.scss'

function Button({backgroundColor, text, onClick}){

    return (
        <button style={{backgroundColor: backgroundColor}} onClick={onClick} className={styles.button}>
            {text}
        </button>
    )
}

export default Button