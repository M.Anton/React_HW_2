import { useEffect, useRef, useState } from 'react';
import styles from './ProductCard.module.scss';
import PropTypes from 'prop-types';

ProductCard.propTypes = {
    id: PropTypes.number.isRequired,
    name: PropTypes.string.isRequired,
    price: PropTypes.number.isRequired,
    imgUrl: PropTypes.string.isRequired,
    color: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
    setNumber: PropTypes.string.isRequired,
    selectedProductsId: PropTypes.array,
    updateSelectedProductsCount: PropTypes.func,
    openAddModal: PropTypes.func,
}

ProductCard.defaultProps = {
    selectedProductsId: [],
    updateSelectedProductsCount: ()=>{},
    openAddModal: ()=>{},
}

function ProductCard(props){
    let {id, name, price, description, imgUrl, color, openAddModal, selectedProductsId, updateSelectedProductsCount} = props;
    const [productSelected, setProductSelected] = useState(false);
    let selectedProducts = useRef([]);
    let selectedProdsId = useRef(selectedProductsId)

    useEffect(() => {
        if (localStorage.getItem('selectedProducts')) {
            selectedProducts.current = JSON.parse(localStorage.getItem('selectedProducts'));
            selectedProdsId.current = JSON.parse(localStorage.getItem('selectedProducts'));
            console.log('INITIAL selectedProductsId ARRAY: ', selectedProdsId.current);
            console.log("Initial selectedProducts.current array ", selectedProducts.current);
            if (selectedProdsId.current.includes(id)) {
                setProductSelected(true);
            }
        }
        else {
            selectedProducts.current = [];
            console.log("Initial selectedProducts.current array ", selectedProducts.current);
        }
    }, []);


    function selectProduct() {
        if (!productSelected) {

            if (localStorage.getItem('selectedProducts')){
                selectedProducts.current = JSON.parse(localStorage.getItem('selectedProducts'));
            }

            selectedProducts.current.push(id);
            localStorage.setItem('selectedProducts', JSON.stringify(selectedProducts.current));
            setProductSelected(true);
 
            selectedProdsId.current = JSON.parse(localStorage.getItem('selectedProducts'));

            updateSelectedProductsCount();
            console.log("selectedProducts.current after product wasn`t selected ", selectedProducts.current);
            console.log('SelectedProdsId ARRAY after product wasn`t selected: ', selectedProdsId.current);
        } else {

            selectedProducts.current = JSON.parse(localStorage.getItem('selectedProducts'));

            let updatedSelectedProducts = selectedProducts.current.filter(product => product !== id);
            console.log(selectedProducts.current);
            selectedProducts.current = updatedSelectedProducts;
            console.log("Filtrated selectedProducts.current after product was selected ", selectedProducts.current)
            localStorage.setItem('selectedProducts', JSON.stringify(updatedSelectedProducts));
            setProductSelected(false);
            console.log("selectedProducts.current after product was selected ", selectedProducts.current);

            selectedProdsId.current = JSON.parse(localStorage.getItem('selectedProducts'));

            updateSelectedProductsCount();
            console.log("SelectedProdsId array after product was selected: ", selectedProdsId.current);
        }
    }

    return (
        <div className={styles.productWrapper}>
            <div className={styles.productInfoContainer}>
                <div className={styles.imgWrapper}>
                    <img className={styles.productImage} src={imgUrl} alt="product" />
                </div>
                <h1 className={styles.productName}>{name}</h1>
                <p className={styles.productDescription}>{description}</p>
                <p className={styles.productColor}>Color: <span>{color}</span></p>
            </div>
            <div className={styles.priceAndBtnWrapper}>
                <p className={styles.productPrice}>${price}</p>
                <button className={styles.buyBtn} onClick={(event)=>openAddModal(event, id)}>
                    Add to cart
                </button>

            <svg onClick={selectProduct}

                className={selectedProdsId.current.includes(id) || productSelected ? `${styles.starIcon} ${styles.selected}` : styles.starIcon }
                
                version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" 
                viewBox="0 0 47.94 47.94">
            <path d="M26.285,2.486l5.407,10.956c0.376,0.762,1.103,1.29,1.944,1.412l12.091,1.757
                c2.118,0.308,2.963,2.91,1.431,4.403l-8.749,8.528c-0.608,0.593-0.886,1.448-0.742,2.285l2.065,12.042
                c0.362,2.109-1.852,3.717-3.746,2.722l-10.814-5.685c-0.752-0.395-1.651-0.395-2.403,0l-10.814,5.685
                c-1.894,0.996-4.108-0.613-3.746-2.722l2.065-12.042c0.144-0.837-0.134-1.692-0.742-2.285l-8.749-8.528
                c-1.532-1.494-0.687-4.096,1.431-4.403l12.091-1.757c0.841-0.122,1.568-0.65,1.944-1.412l5.407-10.956
                C22.602,0.567,25.338,0.567,26.285,2.486z"/>
            </svg>
            </div>
        </div>
    )
}

export default ProductCard